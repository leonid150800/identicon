# frozen_string_literal: true

require 'digest'
require 'mini_magick'

class Identicon
  def initialize(user_name, path = '')
    @user_name = user_name
    @path = path
  end

  def generate
    string = Digest::SHA1.hexdigest @user_name
    bits = string[0..24].each_byte.map { |e| e % 2 }
    color = string[26..31]

    rgb = [color[0...2].hex, color[2...4].hex, color[4...6].hex]

    blob = bits.map { |e| e == 1 ? rgb : [255, 255, 255] }.flatten.pack('C*')

    tmp = Tempfile.new(%w[mini_magick .png])

    MiniMagick::Tool::Convert.new do |convert|
      convert.size '5x5'
      convert.xc 'white'
      convert << tmp.path
    end

    MiniMagick::Image.new(tmp.path, tmp)
    img = MiniMagick::Image.import_pixels(blob, 5, 5, 8, 'rgb')
    img.scale('250x250')
    img.write("#{@path}identicon.png")
  end
end

print "\nEnter username>"
username = gets.chomp

identicon1 = Identicon.new(username)
identicon1.generate

# identicon2 = Identicon.new(username, '/Users/leonid/')
# identicon2.generate
