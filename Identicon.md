
Написать гем/скрипт, который генерирует [Identicon](https://ru.wikipedia.org/wiki/Identicon) по имени пользователя.
Например, такие изображения для аватарок по умолчанию использует гитхаб.

Требования к Идентикону:
  - сетка 5х5 квадратов
  - разрешение 250х250 пикслей
  - для одного и того же имени картинка всегда должна быть одинакового вида и цвета.

Примеры:
![Идентиконы идентикончики](https://github.blog/wp-content/uploads/2013/08/a3c4e2a0-04df-11e3-824c-7378e6550707.png)

Главный класс `Identicon` должен иметь конструктор и один публичный метод `#generate`.

```
## Параметр user_name - строка
## Параметр path - строка пути, куда сохраняется png с идентиконом. Опциональный, по умолчанию сохраняется в текущую папку
identicon = Identicon.new(user_name, path)
identicon.generate # генерирует и сохраняет файл png
```

Для работы с изображениями можно использовать `mini_magick` или любое другое решение.